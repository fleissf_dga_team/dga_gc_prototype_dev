# FLEISS Software Foundation

# DGA MMORPG

DGA is a free/libre and open-source, cross-platform massively multiplayer online role-playing video game featuring weapons. The first type of weapons being realized in DGA is land combat vehicles. DGA is designed to be cross-platform by used technologies and platforms, however, the DGA project aims at providing a full-featured MMORPG on Linux operating system.

## DGA Game Client Prototype, Feature-Complete Version

DGA Game Client Prototype is intended to model, test, and work out the DGA game client's functions and modes running within the jMonkeyEngine game engine only.

## Download & Installation Instructions

For downloading and installing DGA Game Client Prototype accomplish the following steps:

1. Find binary builds of DGA Game Client Prototype in the [Download section](https://bitbucket.org/fleissf_dga_team/dga_gc_prototype_rel/downloads/).

2. Choose a build corresponding to the version of your operating system. Every build includes all necessary components and does not need of any external program. Only the JAR build requires of an external Java Runtime Environment (JRE) that should be already installed in your operating system.

3. A binary build is compressed in a zip archive. After having downloaded the archive can be simply unzipped to the destination folder.

4. After unzipping the archive you find in the destination folder a subfolder called 'DGA-Prototype' which contains an executable with the same name and in the format corresponding to your operating system. If you have installed the JAR build then your executable is called 'DGA-Prototype.jar'.

5. Run the executable by simply clicking it. In the case of the JAR executable please be assured that you have already installed a Java Runtime Environment (JRE) and correctly set the JAVA_HOME environment variable.

6. In the opened window set desired screen settings and click the button 'Continue'. For the sake of stability please use at first the most simple settings possible and only on next restarts turn complex settings on.

7. That's all! You are in game.

